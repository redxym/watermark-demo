# watermark-demo
> 前端添加水印，兼容IE浏览器
> 
> font-end set watermark, Compatible with Internet Explorer
---
**使用（Usage）** 

点击[此处](https://gitee.com/redxym/watermark-demo) 查看使用说明。

**方式一：** 如果过你的页面足够简单，只是一个简单的html静态页面或者是你不想引入太多的package,那么你可以下载[watermark.min.js](https://gitee.com/redxym/watermark-demo/blob/master/dist/watermark.min.js) 
并将其导入到 HTML文件中，这样你就可以使用watermark的这个全局对象来加载水印:

(Download index.js and include it in your HTML document, this will add a global object called watermark)
```
<script src="watermark.min.js"></script>
<script type="text/javascript">
      watermark.setWatermark(
         {
           w_texts: ['第一行水印', '第二行水印'],
         }
      )
</script>
```

**方式二：** 安装npm包导入到需要使用的项目中去：

(Alternatively you can use a JavaScript package manager to add it to your project)
```
npm install watermark-demo --save
   
// import
import watermark from 'watermark-demo'
```

导入后就可以调用下面的方法
（After that you can call any of methods that are explained in the following）

### watermark.setWaterMark()

使用时该方法时传入一个 options 配置对象, 配置对象主要有两个属性：w_texts、w_options。 w_texts：水印文案的数组集合可设置多行文案。 w_options：水印参数配置项，如字体大小、颜色等

You can use the watermark.setWaterMark method to set watermark. The value will automatically be escaped for you.
```
import watermark from 'watermark-demo'
// w_texts：水印文案数组
watermark.setWaterMark(
   {
      w_texts: ['第一行水印', '第二行水印']
   }
)
```
options 配置对象还有另一个属性 w_options水印参数配置： 用于设置水印的具体参数如宽度，大小等，各参数说明见下表

If you need more options, like setting the width, you can add an object with options as the last parameter:
```
import watermark from 'watermark-demo'
watermark.setWaterMark(
  {
    w_texts: ['我是水印呀', '第二行水印'],
    w_options: {
      width: 200,
      height: 100,
      top: '0px',
      left: '0px',
      fontSize: '16',
      fontFamily: 'serif',
      fillStyle: 'rgba(200, 200, 200, 0.8)',
      textAlign: 'left', // 水印文本对齐方向
      rotateDeg: 20,
      zIndex: 999999
    }
   }
);
```
| key | value | default_value |
| --------- | --------- | --------- |
| width| A number that the width of the watermark block. (水印块宽度)| 200
| height| A number that the height of the watermark block. (水印块高度)| 100
| top| A string that the distance between the watermark mask layer and the top of the page.(水印遮罩层距离页面顶部的距离)| '0px'
| left| A string that the distance between the watermark mask layer and the left of the page.(水印遮罩层距离页面左边的距离)| '0px'
| fontSize| A string that the watermark font size(水印的字体大小)| '16'
| fontFamily| A string that the watermark font style(水印的字体风格)| 'serif'
| color| A string that the watermark font color(水印字体颜色)| '#666'
| opacity| AA string that the watermark mask layer transparency(水印遮罩层透明度)| '0.8'
| rotateDeg| A string that specifies SameSite attribute that restricts cookie access based on the site context.(水印角度)| 20
| zIndex| A string that the zIndex of watermark mask(水印遮罩层层级)| 999999

##watermark.removeWatermark
清除水印
clear watermark
```
watermark.removeWatermark()
```

